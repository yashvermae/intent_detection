import json 
import matplotlib.pyplot as plt
import numpy as np
import subprocess

width = 0.35

ls = []
ls = subprocess.check_output("ls", shell=True).split("\n")
ls.pop()
ls.remove("graph.py")
ls.remove("unsupervised_clustering.py")
ls.remove("graphs")
for algo in ls:
    with open(algo, "r") as outfile:
        data = json.load(outfile)

    class1 = []
    class2 = []

    for i in data.keys():
        class1.append(data[i][0])
        class2.append(data[i][1])

    class1 = tuple(class1)
    class2 = tuple(class2)

    ind = np.arange(8)

    p1 = plt.bar(ind, class1, width)
    p2 = plt.bar(ind, class2, width)

    plt.ylabel("number of utterances")
    plt.title("data distribution " + algo[:-5])


    plt.xticks(ind, tuple(data.keys()), rotation=90)
    plt.savefig("./graphs/"+algo[:-5] + ".png")
    plt.show()
