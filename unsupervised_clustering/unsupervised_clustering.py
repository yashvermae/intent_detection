import sys
sys.path.append("../")

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from sklearn.cluster import KMeans
from data import *
import nltk
from sklearn import cluster, mixture
from sklearn.neighbors import kneighbors_graph
from nltk.tokenize import TweetTokenizer
import matplotlib.pyplot as plt
import warnings
import numpy as np

sw = load_sw()
data = load_data()
stemmer = nltk.stem.SnowballStemmer('english')
tt = TweetTokenizer()

X = []
mapping = {}
label_mapping = {}
labels = {}
nu_cluster = 2
cnt = 0
Y = []

for i, ke in enumerate(data.keys()):
    temp_li = []
    for text in data[ke]:
        text = line_clean(text)
        text = line_sw(text, tt, sw)
        text = line_stem(text, tt, stemmer)
        if len(text) >= 10:
            X.append(text)
            Y.append(cnt)
            label_mapping[cnt] = ke
            cnt += 1

vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(X)
X = X.toarray() 
bandwidth = cluster.estimate_bandwidth(X, 0.2)
connectivity = kneighbors_graph(X, 2, include_self=False)
connectivity = 0.5 * (connectivity + connectivity.T)

two_means = cluster.MiniBatchKMeans(n_clusters=2)
ward = cluster.AgglomerativeClustering(
            n_clusters= 2, linkage='ward',
            connectivity=connectivity)

spectral = cluster.SpectralClustering(
        n_clusters=2, eigen_solver='arpack',
        affinity="nearest_neighbors")

average_linkage = cluster.AgglomerativeClustering(linkage="average", affinity="cityblock",n_clusters=2, connectivity=connectivity)

birch = cluster.Birch(n_clusters=2)

gmm = mixture.GaussianMixture(
            n_components=2, covariance_type='full')

kmeans = KMeans(n_clusters = nu_cluster)

clustering_algorithms = (
        ('kmeans', kmeans),
        ('MiniBatchKMeans', two_means),
        ('SpectralClustering', spectral),
        ('Ward', ward),
        ('AgglomerativeClustering', average_linkage),
        ('Birch', birch),
        ('GaussianMixture', gmm)
    )


for name, algorithm in clustering_algorithms:
    
    print "running on algorithm " + name

        # catch warnings related to kneighbors_graph
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            message="the number of connected components of the " +
            "connectivity matrix is [0-9]{1,2}" +
            " > 1. Completing it to avoid stopping the tree early.",
            category=UserWarning)
        warnings.filterwarnings(
            "ignore",
            message="Graph is not fully connected, spectral embedding" +
            " may not work as expected.",
            category=UserWarning)
        algorithm.fit(X)

    if hasattr(algorithm, 'labels_'):
        y_pred = algorithm.labels_.astype(np.int)
    else:
        y_pred = algorithm.predict(X)
    clus = {}
    for ke in data.keys():
        clus[ke] = []
        for i in range(nu_cluster):
            clus[ke].append(0)

    for i,y in enumerate(y_pred):
        clus[label_mapping[i]][y] += 1

    for ke in clus.keys():
        print(ke + " " + str(clus[ke]))

    with open(name+".json", "w+") as outfile:
        json.dump(clus, outfile)
