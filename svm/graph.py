import json 
import matplotlib.pyplot as plt
import numpy as np
import subprocess

X = [0.5, 0.75, 1, 1.25, 1.5]

Y = [28.12, 30.4, 31.37, 36.69, 31.48]

Y_scale = [32.03, 35.07, 36.26492942453854, 36.69, 36.69]

plt.xlabel("C")
plt.ylabel("accuracies")

plt.title("variation of accuracies by varying parameter C")

plt.plot(X, Y, color="blue", label="auto_deprecated")
plt.plot(X, Y_scale, color="orange", label="scale")
plt.legend()
plt.show()
