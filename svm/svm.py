import sys
sys.path.append('../')

import os
import numpy as np
import pandas as pd
from data import *
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical
import nltk
from nltk.tokenize import TweetTokenizer
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm
sw = load_sw()
data = load_data()

tt = TweetTokenizer()
stemmer = nltk.stem.SnowballStemmer('english')

X = []
Y = []

for i, k in enumerate(data.keys()):
    for text in data[k]:
        text = line_clean(text)
        text = line_sw(text, tt, sw)
        text = line_stem(text, tt, stemmer)
        X.append(text)
        Y.append(i)

data = pd.DataFrame(
        {
            'text': X,
            'label': Y
            })

max_features = 1000
tokenizer = Tokenizer(num_words=max_features, split=' ')
tokenizer.fit_on_texts(data['text'].values)
X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X)


#Y = pd.get_dummies(data['label']).values
X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.20, random_state = 5)
X = []
for i in range(len(X_test)):
    X.append(X_test)

C = [1.0, 0.5, 0.75, 1.25, 1.5]
kernel = ['linear', 'poly', 'rbf']
gamma = ['auto_deprecated', 'scale']
for i in range(len(C)):
    for j in range(len(kernel)):
        for k in range(len(gamma)):

            print "C : " + str(C[i]) + " kernel : " + kernel[j] + " gamma : " + gamma[k]
            clf = svm.SVC(C = C[i], gamma=gamma[k], decision_function_shape='ovo', kernel=kernel[j])
            clf.fit(X_train, Y_train)
            x = clf.predict(X[i])
            corr = 0
            for m in range(len(X)):
                if x[m] == Y_test[m]:
                    corr += 1
            total = len(X)
            accu = float(corr) / float(total)
            print "Model : " + str(C[i]) + " " + kernel[j] + " " + gamma[k] + "\naccu " + str(accu * 100)
