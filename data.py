import os
import sys
import json
import subprocess
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
import numpy as np

def make_json_dataset():
    files = subprocess.check_output("ls ./dataset", shell=True)
    files = files.decode("utf-8")
    files = files.split("\n")
    files.pop()
    data = {}
    for _file in files:
        file_data = open("./dataset/" + _file).read().split("\n")
        _file = _file[:-4]
        data[_file] = file_data
    with open("dataset.json", "w+") as outfile:
        json.dump(data, outfile)

def make_json_stopWords():
    stop_word_list = stopwords.words("english")
    stop_word_dic = {}
    for sw in stop_word_list:
        stop_word_dic[sw] = True
    with open("stop_word.json", "w+") as outfile:
        json.dump(stop_word_dic, outfile)

def load_data():
    with open("/home/yash/sem/7/btp2/dataset.json", "r") as outfile:
        data = json.load(outfile)
    return data

def load_sw():
    with open("/home/yash/sem/7/btp2/stop_word.json", "r") as outfile:
        sw = json.load(outfile)
    return sw

def line_clean(line):
    line = line.encode('utf-8').decode("unicode_escape")
    line = line.strip("\n")
    line = line.strip("\r")
    line = line.strip("\r\n")
    line = line.strip()
    line = line.lower()
    line = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','_URL_',line)
    line = re.sub(r'[^\w\s]', '', line)
    line = re.sub('[\s]+', ' ', line)
    return line.lower()

def line_sw(line, tt, sw):
    line = tt.tokenize(line)
    temp_li = []
    for w in line:
        if w not in sw:
            temp_li.append(w)
    line = ' '.join(temp_li)
    return line

def line_stem(line, tt, stemmer):
    line = tt.tokenize(line)
    for i,w in enumerate(line):
        line[i] = stemmer.stem(w)
    line = ' '.join(line)
    return line

def create_vector(line, model, dim):
    tmp_vec = np.zeros(dim)
    if len(line) == 0:
        return tmp_vec
    for word in line:
        try:
            tmp_vec += model.wv[word]
        except:
            pass
    tmp_vec = tmp_vec/float(len(line))
    return tmp_vec

if __name__ == '__main__':
    make_json_dataset()
    make_json_stopWords()

