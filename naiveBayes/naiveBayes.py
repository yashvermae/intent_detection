import sys
sys.path.append('../')

import os
import numpy as np
import pandas as pd
from data import *
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from sklearn.model_selection import train_test_split
import nltk
from nltk.tokenize import TweetTokenizer
from matplotlib import pyplot
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix, roc_auc_score, recall_score, precision_score

def report_results(model, X, y):
    pred_proba = model.predict_proba(X)[:, 1]
    pred = model.predict(X)        
    #auc = roc_auc_score(y, pred_proba)
    acc = accuracy_score(y, pred)
    f1 = f1_score(y, pred)
    prec = precision_score(y, pred)
    rec = recall_score(y, pred)
    result = {'auc': auc, 'f1': f1, 'acc': acc, 'precision': prec, 'recall': rec}
    return result

sw = load_sw()
data = load_data()

tt = TweetTokenizer()
stemmer = nltk.stem.SnowballStemmer('english')

X = []
Y = []

for i, k in enumerate(data.keys()):
    for text in data[k]:
        text = line_clean(text)
        text = line_sw(text, tt, sw)
        text = line_stem(text, tt, stemmer)
        X.append(text)
        Y.append(i)

data = pd.DataFrame(
        {
            'text': X,
            'label': Y
            })

max_features = 1000
tokenizer = Tokenizer(num_words=max_features, split=' ')
tokenizer.fit_on_texts(data['text'].values)
X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X)
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3)


clf1 = MultinomialNB()
clf1.fit(X_train, Y_train)
pred = clf1.predict(X_test)
print pred

cor = 0
total = len(X_test)
for i in range(len(X_test)):
    print str(pred[i]) + " " + str(Y_test[i])
    if pred[i] == Y_test[i]:
        cor += 1

print("accuracy is " + str(float(cor)/float(total) * 100))

