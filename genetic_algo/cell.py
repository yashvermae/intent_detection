# population size -> 10
# selection -> best 3 + 2 randomly from left over
import random
import sys
import os
import subprocess
import copy
from bilstm import *

gen = 3
total_pop = 10
nc2 = 5
star_pop = 3
avg_pop = 2

gene_dic = {0:"layer_data", 
        1:"act_func", 
        2:"embed_dim", 
        3:"max_feat", 
        4:"opti"}

def gen_random(l, r):
    return random.randint(l, r)

def gen_layer_data():
    print "generating layer data"
    num_layers = gen_random(1, 3)
    li = []
    for i in range(num_layers):
        li.append(str(gen_random(100, 500)))
    s = str(num_layers) + ","
    s += ','.join(li)
    return s

def gen_act_func():
    return gen_random(0,3)

def gen_embed_dim():
    return gen_random(100, 500)

def gen_max_feat():
    return gen_random(500, 2000)

def gen_opti():
    return gen_random(0,5)

gene_mutate_func = {0 : gen_layer_data,
                    1 : gen_act_func,
                    2 : gen_embed_dim,
                    3 : gen_max_feat,
                    4 : gen_opti}


class Genetic_obj():
    def __init__(self, gene=None):
        if gene == None:
            self.gene = {}
    def initialize(self):
        for i in range(5):
            self.gene[i] = gene_mutate_func[i]()
        # list-[num of layers, neurons in each layer]
        # activation function 
        # value of embedding dimension
        # number of maximum features
        #  dense layer optimization function

def print_gen_info(obj):
    print str(obj.gene)

def mutate(a):
    gen_mutate = gen_random(0, 4)
    a.gene[gen_mutate] = gene_mutate_func[gen_mutate]()
    return a

def selection(a, b):
    c = Genetic_obj()  # c -> child of a, b after selection
    for i in range(5):
        rn = gen_random(0,1)
        if rn == 0:
            c.gene[i] = a.gene[i]
        else:
            c.gene[i] = b.gene[i]
    return c                

def fitness_func(obj):
    '''
        evaluates how good an obj is by first running the code with given parameters, 
        finds the point where validation loss is less than the train loss
        runs again the code with that many epochs and return the accu on the test data
    '''
    #iter_epoch = bilstm(obj.gene[0], obj.gene[1], obj.gene[2], obj.gene[3], obj.gene[4], 5, 0)
    score_epoch = bilstm(obj.gene[0], obj.gene[1], obj.gene[2], obj.gene[3], obj.gene[4], 3, 1)
    return float(score_epoch)

def evaluate_generation(gen): # will return a list of tuples
    li = []
    for obj in gen:           # (obj, accuracy)
        score = fitness_func(obj)
        li.append((obj, score))
    return li
        

def gen_generation(prev_gen):
    results = evaluate_generation(prev_gen)
    results.sort(key=lambda x:float(x[1]), reverse=True)
    best = results[:star_pop]
    avg = []
    for i in range(avg_pop):
        avg.append(results[gen_random(star_pop, total_pop-1)])
    chosen = best + avg
    print "chosen greats of generation : " + str(gen)
    for i in range(len(chosen)):
        print str(chosen[i][0].gene) + " " + str(chosen[i][1])
    new_gen = []
    for i in range(len(chosen)):
        for j in range(i+1, len(chosen)):
            obj1 = chosen[i]
            obj2 = chosen[j]
            new_gen.append(selection(obj1[0], obj2[0]))
    num_to_mutate = gen_random(1, total_pop - 1)
    chosen_to_mutate = []
    for i in range(num_to_mutate):
        chosen_to_mutate.append(gen_random(0, total_pop - 1))
    for i in range(num_to_mutate):
        tmp = chosen_to_mutate[i]
        new_gen[tmp] = mutate(new_gen[tmp])
    return new_gen

def gen_random_obj():
    tmp = Genetic_obj()
    tmp.initialize()
    return tmp

if __name__ == "__main__":
    prev_gen = []
    for i in range(total_pop):
        prev_gen.append(gen_random_obj())
    for i in range(gen + 1):
        curr_gen = gen_generation(prev_gen)
        prev_gen = curr_gen
