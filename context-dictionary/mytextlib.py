import sys
import os
import subprocess
from nltk.stem.wordnet import WordNetLemmatizer
import nltk
import string
import re
import numpy as np
from html.parser import HTMLParser

def load_stop_words():
    stop_words = {}
    temp_stop_words = open("/home/yash/adobe/baseline-IR/stop_words.txt", "r")
    temp_stop_words = temp_stop_words.read().split("\n")
    for word in temp_stop_words:
        stop_words[word] = True
    return stop_words

def load_stop_symbols():
    stop_sym = {}
    temp_stop_sym = open("/home/yash/adobe/baseline-IR/stop_symbols.txt", "r")
    temp_stop_sym = temp_stop_sym.read().split("\n")
    for word in temp_stop_sym:
        stop_sym[word] = True
    return stop_sym

def load_data():
    files = subprocess.check_output("ls /home/yash/adobe/cluster/data_class/", shell=True)
    files = files.decode("utf-8")
    files = files.split("\n")
    files.pop()
    data = {}
    for _file in files:
        file_data = open("/home/yash/adobe/cluster/data_class/" + _file).read().split("\n")
        _file = _file[:-4]
        data[_file] = file_data
    return data

def clean_line(text, lemma, stemmer, stop_words, html_parser, flag):
    text = text.lower()
    text = text.strip()
    text = html_parser.unescape(text)
    text = text.replace('\'', '')
    text = re.sub('[\d]', '', text)
    text = re.sub('[^\w]', ' ', text)
    words = nltk.word_tokenize(text)
    li = []
    for word in words:
        if word not in stop_words:
            word = lemma.lemmatize(word)
            if flag == 1:
                word = stemmer.stem(word)
            if len(word) > 1:
                li.append(word)
    text = ' '.join(li)
    return text
    
