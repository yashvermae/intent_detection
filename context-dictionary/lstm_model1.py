import sys
import os
import numpy as np
import pandas as pd
from mytextlib import *
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix, roc_auc_score, recall_score, precision_score
from nltk.tokenize import TweetTokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from keras.utils.np_utils import to_categorical
import re
import numpy as np
import json


stop_words = load_stop_words()
with open("res.json", "r") as outfile:
    res = json.load(outfile)

p_data = []
n_data = []

with open("positive_phrase.json", "r") as outfile:
    p_data = json.load(outfile)
with open("negative_phrase.json", "r") as outfile:
    n_data = json.load(outfile)

lemma = WordNetLemmatizer()
ps = nltk.stem.SnowballStemmer('english')
html_parser = HTMLParser()

def tokenize(text): 
    tknzr = TweetTokenizer()
    return tknzr.tokenize(text)

def report_results(model, X, y):
    pred_proba = model.predict_proba(X)[:, 1]
    pred = model.predict(X)        
    auc = roc_auc_score(y, pred_proba)
    acc = accuracy_score(y, pred)
    f1 = f1_score(y, pred)
    prec = precision_score(y, pred)
    rec = recall_score(y, pred)
    result = {'auc': auc, 'f1': f1, 'acc': acc, 'precision': prec, 'recall': rec}
    return result


X1 = []
Y1 = []

for i in range(len(p_data)):
    text = p_data[i]
    text = clean_line(text, lemma, ps, stop_words, html_parser, 1)
    X1.append(text)
    Y1.append(1)

for i in range(len(n_data)):
    text = n_data[i]
    text = clean_line(text, lemma, ps, stop_words, html_parser, 1)
    X1.append(text)
    Y1.append(0)

data1 = pd.DataFrame(
        {
            'text': X1,
            'label': Y1
            })

max_features = 2000
tokenizer1 = Tokenizer(num_words=max_features, split=' ')
tokenizer1.fit_on_texts(data1['text'].values)
X1 = tokenizer1.texts_to_sequences(data1['text'].values)
X1 = pad_sequences(X1)

embed_dim = 196
lstm_out = 128

model1 = Sequential()
model1.add(Embedding(max_features, embed_dim,input_length = X1.shape[1]))
model1.add(SpatialDropout1D(0.4))
model1.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
model1.add(Dense(2,activation='softmax'))
model1.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])


Y1 = pd.get_dummies(data1['label']).values
X1_train, X1_test, Y1_train, Y1_test = train_test_split(X1,Y1, test_size = 0.30, random_state = 5)

print("Model 1")
print(model1.summary())
print(X1_train.shape,Y1_train.shape)
print(X1_test.shape,Y1_test.shape)

batch_size = 32
model1.fit(X1_train, Y1_train, epochs = 2, batch_size=batch_size, verbose = 2)

validation_size = 800

X1_validate = X1_test[-validation_size:]
Y1_validate = Y1_test[-validation_size:]
X1_test = X1_test[:-validation_size]
Y1_test = Y1_test[:-validation_size]
score,acc = model1.evaluate(X1_test, Y1_test, verbose = 2, batch_size = batch_size)

print("score: %.2f" % (score))
print("acc: %.2f" % (acc))

f_cnt, o_cnt, f_correct, o_correct = 0, 0, 0, 0
for x in range(len(X1_validate)):
    result = model1.predict(X1_validate[x].reshape(1,X1_test.shape[1]),batch_size=1,verbose = 2)[0]
    if np.argmax(result) == np.argmax(Y1_validate[x]):
        if np.argmax(Y1_validate[x]) == 0:
            o_correct += 1
        else:
            f_correct += 1
                                                                
    if np.argmax(Y1_validate[x]) == 0:
        o_cnt += 1
    else:
        f_cnt += 1

print("focus : " + str(f_correct) + "/" + str(f_cnt) + " , " + str(f_correct/f_cnt*100))
print("other : " + str(o_correct) + "/" + str(o_cnt) + " , " + str(o_correct/o_cnt*100))



'''
vectorizer = CountVectorizer(
            analyzer = 'word',
            tokenizer = tokenize,
            lowercase = True,
            ngram_range=(1, 3),
            stop_words = stop_words)

kfolds = StratifiedKFold(n_splits=10, shuffle=True, random_state=1)

np.random.seed(5)

pipeline_svm = make_pipeline(vectorizer, SVC(probability=True, kernel="linear", class_weight="balanced"))

grid_svm = GridSearchCV(pipeline_svm, param_grid = {'svc__C': [0.01, 0.1, 1]}, cv = kfolds, scoring="roc_auc", verbose=1, n_jobs=4) 

grid_svm.fit(X1_train, Y1_train)
grid_svm.score(X1_test, Y1_test)


print(grid_svm.best_params_)
print(grid_svm.best_score_)

print(report_results(grid_svm.best_estimator_, X1_test, Y1_test))

pipeline_svm = make_pipeline(vectorizer, SVC(probability=True, kernel="linear", class_weight="balanced"))

grid_svm = GridSearchCV(pipeline_svm, param_grid = {'svc__C': [0.01, 0.1, 1]}, cv = kfolds, scoring="roc_auc", verbose=1, n_jobs=4) 

grid_svm.fit(X2_train, Y2_train)
grid_svm.score(X2_test, Y2_test)


print(grid_svm.best_params_)
print(grid_svm.best_score_)

print(report_results(grid_svm.best_estimator_, X2_test, Y2_test))
'''

