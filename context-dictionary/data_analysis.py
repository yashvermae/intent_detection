from mytextlib import *
from sklearn.metrics.pairwise import cosine_similarity
from nltk.corpus import brown
import gensim
from html.parser import HTMLParser
from gensim.models import Word2Vec
from nltk.corpus import wordnet
import json
dcy = {}
res = {}
data = open("dictionary.csv", "r").read().split("\n")

for line in data:
    line = line.split(",")
    try:
        word = line[0]
        gram = line[1]
        mean = line[2]
    except:
        pass
    word = word.strip("\"").lower()
    gram = gram.strip("\"")
    mean = mean.strip("\"")
    word = word.strip("\'")
    if word not in dcy:
        dcy[word] = []
    if gram not in dcy[word]:
        dcy[word].append(gram)

stop_words = load_stop_words()
stop_symbols = load_stop_symbols()
html_parser = HTMLParser()
lemma = WordNetLemmatizer()
ps = nltk.stem.SnowballStemmer('english')
n_words = len(dcy.keys())
print("Number of words in dictionary are " + str(n_words))
li = ["good", "beneficial", "hello", "phone", "pleasant", "bark", "nails",\
        "jam", "pool", "nail"]
for word in dcy.keys():
    if word in stop_words:
        continue
    try:
        syns_o = wordnet.synsets(word)
        sim_words = set()
        for i in range(len(syns_o)):
            sim_words.add(syns_o[i].lemmas()[0].name())
        for i in sim_words:
            syns_t = wordnet.synsets(i)
            temp_set = set()
            for j in range(len(syns_t)):
                temp_set.add(syns_t[j].lemmas()[0].name())
            if len(syns_t) < 3:
                if word not in res:
                    res[word] = []
                res[word].append(i)
                #print(":"+word+": " + i + " "  + str(dcy[i]))
    except Exception as err:
        print(err)

cnt_no = 0
cnt_yes = 0
cnt_diff = 0
for word in res:
    if len(res[word]) == 0:
        cnt_no += 1
    else:
        if len(res[word]) >= 1:
            cnt_yes+= 1
        if len(res[word])  >= 1 and word in dcy.keys():
            cnt_diff += 1

print(cnt_no)
print("number of words with atleast one entry : " + str(cnt_yes))
print("number of words with atleast one entry and in dictionary : " + str(cnt_diff))
print("total number of entries : " + str(len(res)))

with open("res.json", "w+") as outfile:
    json.dump(res, outfile)
with open("dcy.json", "w+") as outfile:
    json.dump(dcy, outfile)
