import json
import requests
from nltk.corpus import wordnet
import subprocess
with open("../dcy.json", "r") as outfile:
    dcy = json.load(outfile)
files = subprocess.check_output("ls", shell=True).decode('utf-8').split("\n")
do = set()

for i, f in enumerate(files):
    files[i] = f[:-5]
    files[i] = files[i].lower()
    do.add(files[i])
s = set()

for word in dcy:
    s.add(word)
    li = wordnet.synsets(word)
    for syn in li:
        name = syn.lemmas()[0].name()
        s.add(name)
cnt = 5
p = set()
p.add("download_stuff.py")
s = s-do
s = s-p
cnt = len(s)
print(str(cnt) + " wikipedia pages will be download")
for word in s:
    try:
        word = word.split(" ")
        word = '_'.join(word)
        if(len(word) == 0):
            continue
        f = word[0]
        l = word[1:]
        word = f.upper() + l
        print("Downloading " + word + " : " + str(cnt) + " ",)
        cnt -= 1
        res = requests.get("https://en.wikipedia.org/wiki/"+word)
        if res.status_code == requests.codes.ok:
            fp = open("./exp/"+word+".html", "w+")
            fp.write(res.text)
            print("Success")
        else:
            print("Failure")
    except:
        print("Error")
