import sys
import os
import numpy as np
import pandas as pd
from mytextlib import *
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix, roc_auc_score, recall_score, precision_score
from nltk.tokenize import TweetTokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from keras.utils.np_utils import to_categorical
import re
import numpy as np
import json
from compare import *
import nltk

stop_words = load_stop_words()
with open("res.json", "r") as outfile:
    res = json.load(outfile)

p_data = []
n_data = []

with open("positive_phrase.json", "r") as outfile:
    p_data = json.load(outfile)
with open("negative_phrase.json", "r") as outfile:
    n_data = json.load(outfile)

lemma = WordNetLemmatizer()
ps = nltk.stem.SnowballStemmer('english')
html_parser = HTMLParser()

def tokenize(text): 
    tknzr = TweetTokenizer()
    return tknzr.tokenize(text)

def report_results(model, X, y):
    pred_proba = model.predict_proba(X)[:, 1]
    pred = model.predict(X)        
    auc = roc_auc_score(y, pred_proba)
    acc = accuracy_score(y, pred)
    f1 = f1_score(y, pred)
    prec = precision_score(y, pred)
    rec = recall_score(y, pred)
    result = {'auc': auc, 'f1': f1, 'acc': acc, 'precision': prec, 'recall': rec}
    return result



X2 = []
Y2 = []
cc = 0
for i in range(len(p_data)):
    text = p_data[i]
    text = clean_line(text, lemma, ps, stop_words, html_parser, 0)
    sentence = text

    text = tokenize(text)
    for i, word in enumerate(text):
        li_words = []
        flag = 0
        if word in res:
            for w in res[word]:
                li_words.append(w)
            flag = 1
            tw = compare_fn(li_words, sentence, w2v_model, stop_words)
            if tw == "-1":
                text[i] = res[word][0]
            else:
                cc += 1
                text[i] = tw
    text2 = ' '.join(text)
    text2 = clean_line(text2, lemma, ps, stop_words, html_parser, 1)
    X2.append(text2)
    Y2.append(1)

for i in range(len(n_data)):
    text = n_data[i]
    text = clean_line(text, lemma, ps, stop_words, html_parser, 0)

    text = tokenize(text)
    for i, word in enumerate(text):
        li_words = []
        if word in res:
            for w in res[word]:
                li_words.append(w)
            tw = compare_fn(li_words, sentence, w2v_model, stop_words)
            if tw == "-1":
                text[i] = res[word][0]
            else:
                cc += 1
                text[i] = tw
    text2 = ' '.join(text)
    text2 = clean_line(text2, lemma, ps, stop_words, html_parser, 1)
    X2.append(text2)
    Y2.append(0)
print("cc is " + str(cc))
data2 = pd.DataFrame(
        {
            'text': X2,
            'label': Y2
            })

max_features = 2000

tokenizer2 = Tokenizer(num_words=max_features, split=' ')
tokenizer2.fit_on_texts(data2['text'].values)
X2 = tokenizer2.texts_to_sequences(data2['text'].values)
X2 = pad_sequences(X2)


embed_dim = 196
lstm_out = 128


model2 = Sequential()
model2.add(Embedding(max_features, embed_dim,input_length = X2.shape[1]))
model2.add(SpatialDropout1D(0.4))
model2.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
model2.add(Dense(2,activation='softmax'))
model2.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])


Y2 = pd.get_dummies(data2['label']).values
X2_train, X2_test, Y2_train, Y2_test = train_test_split(X2,Y2, test_size = 0.30, random_state = 5)


print("Model 2")
print(model2.summary())
print(X2_train.shape,Y2_train.shape)
print(X2_test.shape,Y2_test.shape)

batch_size = 32
model2.fit(X2_train, Y2_train, epochs = 3, batch_size=batch_size, verbose = 2)

validation_size = 800


X2_validate = X2_test[-validation_size:]
Y2_validate = Y2_test[-validation_size:]
X2_test = X2_test[:-validation_size]
Y2_test = Y2_test[:-validation_size]
score,acc = model2.evaluate(X2_test, Y2_test, verbose = 2, batch_size = batch_size)

print("score: %.2f" % (score))
print("acc: %.2f" % (acc))

f_cnt, o_cnt, f_correct, o_correct = 0, 0, 0, 0
for x in range(len(X2_validate)):
    result = model2.predict(X2_validate[x].reshape(1,X2_test.shape[1]),batch_size=1,verbose = 2)[0]
    if np.argmax(result) == np.argmax(Y2_validate[x]):
        if np.argmax(Y2_validate[x]) == 0:
            o_correct += 1
        else:
            f_correct += 1
                                                                
    if np.argmax(Y2_validate[x]) == 0:
        o_cnt += 1
    else:
        f_cnt += 1

print("focus : " + str(f_correct) + "/" + str(f_cnt) + " , " + str(f_correct/f_cnt*100))
print("other : " + str(o_correct) + "/" + str(o_cnt) + " , " + str(o_correct/o_cnt*100))










'''
vectorizer = CountVectorizer(
            analyzer = 'word',
            tokenizer = tokenize,
            lowercase = True,
            ngram_range=(1, 3),
            stop_words = stop_words)

kfolds = StratifiedKFold(n_splits=10, shuffle=True, random_state=1)

np.random.seed(5)

pipeline_svm = make_pipeline(vectorizer, SVC(probability=True, kernel="linear", class_weight="balanced"))

grid_svm = GridSearchCV(pipeline_svm, param_grid = {'svc__C': [0.01, 0.1, 1]}, cv = kfolds, scoring="roc_auc", verbose=1, n_jobs=4) 

grid_svm.fit(X1_train, Y1_train)
grid_svm.score(X1_test, Y1_test)


print(grid_svm.best_params_)
print(grid_svm.best_score_)

print(report_results(grid_svm.best_estimator_, X1_test, Y1_test))

pipeline_svm = make_pipeline(vectorizer, SVC(probability=True, kernel="linear", class_weight="balanced"))

grid_svm = GridSearchCV(pipeline_svm, param_grid = {'svc__C': [0.01, 0.1, 1]}, cv = kfolds, scoring="roc_auc", verbose=1, n_jobs=4) 

grid_svm.fit(X2_train, Y2_train)
grid_svm.score(X2_test, Y2_test)


print(grid_svm.best_params_)
print(grid_svm.best_score_)

print(report_results(grid_svm.best_estimator_, X2_test, Y2_test))
'''

