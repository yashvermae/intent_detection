from nltk.corpus import brown
import json
import subprocess
import gensim
import bs4
import nltk
import os

files = subprocess.check_output("ls ../exp/", shell=True).decode('utf-8').split("\n")
data = {}
dataset = []
cnt = len(files)
print("adding wiki data...")
for f in files:
    try:
        print(cnt)
        cnt -= 1
        temp_data = open("../exp/"+f).read()
        word = f.lower()
        word = word[:-5]
        soup = bs4.BeautifulSoup(temp_data)
        li = soup.select("p")
        text = ""
        for x in li:
            x = x.text
            x = x.strip("\n")
            x = x.lower()
            text += x + " "
        text = text.split(".")
        for sentence in text:
            sentence = nltk.word_tokenize(sentence)
            dataset.append(sentence)
    except Exception as err:
        print(err)
print("adding brown data...")
for b in brown.sents():
    dataset.append(b)

if not os.path.isfile("w2v_model.sav"):
    model_w2v = gensim.models.Word2Vec(dataset, min_count=2, workers=6)
    model_w2v.save("w2v_model.sav")
    print("model created and saved")
