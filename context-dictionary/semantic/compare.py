import re
from sklearn.metrics.pairwise import cosine_similarity
import gensim
import nltk
import numpy as np
import bs4

w2v_model = gensim.models.Word2Vec.load("w2v_model.sav")
def find_w2v(s, model, stop_words):
    s1 = nltk.word_tokenize(s)
    f_s1 = []
    for word in s1:
        if word in stop_words:
            pass
        else:
            f_s1.append(word)
    s1 = f_s1
    vec1 = np.zeros((1, 100))
    for word in s1:
        try:
            temp = model[word]
        except:
            temp = np.zeros((1, 100))
        vec1 += temp
    vec1 = vec1/len(s1)
    return vec1

def find_simi(s1, s2):
    score = cosine_similarity(s1, s2)
    score = score.tolist()
    score = score[0][0]
    return score

def compare_fn(li, sentence, model, stop_words):
    URL = "/home/yash/adobe/context-dictionary/exp/"
    scores = []
    tar_vec = find_w2v(sentence, model, stop_words)
    for i, word in enumerate(li):
        st = word
        word = word.lower()
        f = word[0]
        l = word[1:]
        word = f.upper() + l
        word = word.split(" ")
        word = '_'.join(word)
        try:
            temp_data = open(URL + word+".html", "r").read()
        except:
            continue
        soup = bs4.BeautifulSoup(temp_data, "lxml")
        x = soup.select("p")
        for i in x:
            i = i.text
            try:
                i = i.strip("\n")
            except:
                print("skipped")
                continue
            #print("word is \"" + str(i.text)+"\"")
            if i == None or len(i) == 0:
                continue
            if len(i) >= 10:
                temp_data = i
                break
        temp_data = re.sub('[\d]', '', temp_data)
        temp_data = re.sub('[^\w]', '', temp_data)
        scores.append((st.strip("\n"), find_simi(tar_vec, find_w2v(temp_data, model, stop_words))))
    scores.sort(key=lambda x:float(x[1]), reverse=True)
    try:
        rel_word = scores[0][0]
    except:
        rel_word = "-1"
    return rel_word
