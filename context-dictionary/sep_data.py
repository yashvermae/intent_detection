import sys
import os
import json

data = open("train.tsv", "r").read().split("\n")
data = data[1:]
p = []
n = []
cnt = 0
for line in data:
    try:
        line = line.split("\t")
        sentiment_score = line[3]
        phrase = line[2]
        if len(phrase) < 20:
            continue
        sentiment_score = int(sentiment_score)
        if(sentiment_score == 0):
            n.append(phrase)
        if(sentiment_score == 4):
            p.append(phrase)
    except:
        pass

print("p : " + str(len(p)))
print("n : " + str(len(n)))

print("Some positive examples : ")
print(p[:5])
print("Some negative examples : ")
print(n[:5])

p = p[:6000]
n = n[:6000]

with open("positive_phrase.json", "w+") as outfile:
    json.dump(p, outfile)
with open("negative_phrase.json", "w+") as outfile:
    json.dump(n, outfile)
