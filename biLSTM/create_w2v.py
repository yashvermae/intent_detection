import sys
sys.path.append("../")
from nltk.tokenize import TweetTokenizer
from gensim.test.utils import get_tmpfile
from gensim.models import Word2Vec
from data import *

tt = TweetTokenizer()

print "preprocessing ..."

data = load_data()
X = []
for ke in data.keys():
    for text in data[ke]:
        X.append(tt.tokenize(line_clean(text)))

print "creating vectors ..."

path = get_tmpfile("word2vec.model")
model = Word2Vec(X, size=100, window=5, min_count=3, workers=4)
model.save("word2vec.model")

print "process completed ..."
