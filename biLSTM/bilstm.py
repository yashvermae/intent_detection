import sys
sys.path.append('../')

import os
import numpy as np
import pandas as pd
from data import *
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers.recurrent import GRU
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D, Bidirectional
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical
from keras.layers.core import Dropout
import nltk
from nltk.tokenize import TweetTokenizer
from matplotlib import pyplot

sw = load_sw()
data = load_data()

tt = TweetTokenizer()
stemmer = nltk.stem.SnowballStemmer('english')

X = []
Y = []

for i, k in enumerate(data.keys()):
    for text in data[k]:
        text = line_clean(text)
        text = line_sw(text, tt, sw)
        text = line_stem(text, tt, stemmer)
        X.append(text)
        Y.append(i)

data = pd.DataFrame(
        {
            'text': X,
            'label': Y
            })

max_features = 1000
tokenizer = Tokenizer(num_words=max_features, split=' ')
tokenizer.fit_on_texts(data['text'].values)
X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X)

embed_dim = 196
lstm_out = 128

model = Sequential()
model.add(Embedding(max_features, embed_dim,input_length = X.shape[1]))
model.add(SpatialDropout1D(0.4))
model.add(Bidirectional(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2)))
model.add(Dense(8,activation='softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])
print(model.summary())

Y = pd.get_dummies(data['label']).values
X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.20, random_state = 5)
print(X_train.shape,Y_train.shape)
print(X_test.shape,Y_test.shape)

batch_size = 128
history = model.fit(X_train, Y_train, epochs = 5, batch_size=batch_size, verbose = 2, validation_split=0.2)

score,acc = model.evaluate(X_test, Y_test, verbose = 2, batch_size = batch_size)

train_g = history.history['loss']
val_g = history.history['val_loss']

pyplot.plot(train_g, color='blue', label = 'train')
pyplot.plot(val_g, color='orange', label = 'validation')
pyplot.ylabel("loss")
pyplot.xlabel("epoch")
pyplot.show()

print("score: %.2f" % (score))
print("acc: %.2f" % (acc))
