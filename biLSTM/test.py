from gensim.models import Word2Vec
import numpy as np
import nltk
from nltk.tokenize import TweetTokenizer

model = Word2Vec.load("word2vec.model")
line = "hello my computer"

tt = TweetTokenizer()

line = tt.tokenize(line)
vec = model.wv["computer"]
print vec.shape

mat = np.zeros(100)
print mat.shape

for word in line:
    try:
        mat += model.wv[word]
    except:
        pass
    print mat
mat = mat/len(line)
print mat.shape[0]
print mat
