import sys
sys.path.append('../')

import os
import numpy as np
import pandas as pd
from data import *
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers.recurrent import GRU
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D, Bidirectional
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical
from keras.layers.core import Dropout
import nltk
from nltk.tokenize import TweetTokenizer
from matplotlib import pyplot
import json
sw = load_sw()
data = load_data()

tt = TweetTokenizer()
stemmer = nltk.stem.SnowballStemmer('english')

X = []
Y = []

max_features = 1000
embed_dim = 196
lstm_out = 128
curr_activation="softmax"
curr_opti = "adam"

for i, k in enumerate(data.keys()):
    for text in data[k]:
        text = line_clean(text)
        text = line_sw(text, tt, sw)
        text = line_stem(text, tt, stemmer)
        X.append(text)
        Y.append(i)

data = pd.DataFrame(
        {
            'text': X,
            'label': Y
            })

tokenizer = Tokenizer(num_words=max_features, split=' ')
tokenizer.fit_on_texts(data['text'].values)
X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X)
Y = pd.get_dummies(data['label']).values


def gru(max_features, embed_dim, lstm_out, curr_activation, curr_opti):
    model = Sequential()
    model.add(Embedding(max_features, embed_dim,input_length = X.shape[1]))
    model.add(SpatialDropout1D(0.4))
    model.add(GRU(lstm_out, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(8,activation=curr_activation))
    model.compile(loss = 'categorical_crossentropy', optimizer=curr_opti,metrics = ['accuracy'])
    print(model.summary())

    X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.20, random_state = 5)
    print(X_train.shape,Y_train.shape)
    print(X_test.shape,Y_test.shape)

    batch_size = 128
    history = model.fit(X_train, Y_train, epochs = 5, batch_size=batch_size, verbose = 2, validation_split=0.2)

    score,acc = model.evaluate(X_test, Y_test, verbose = 2, batch_size = batch_size)

    train_g = history.history['loss']
    val_g = history.history['val_loss']

    pyplot.plot(train_g, color='blue', label = 'train')
    pyplot.plot(val_g, color='orange', label = 'validation')
    pyplot.ylabel("loss")
    pyplot.xlabel("epoch")
    file_name = str(max_features) + "_" + str(embed_dim) + "_" + str(lstm_out) + "_" + str(curr_activation) + "_" + str(curr_opti)
    pyplot.title(file_name)
    pyplot.savefig(file_name + ".png")

    return acc

X_ = []
Y_ = []

max_features = 1000
embed_dim = 196
lstm_out = 128
curr_activation="softmax"
curr_opti = "adam"

for i in range(1000, 2000, 100):
    max_features = int(i)
    accu = gru(max_features, embed_dim, lstm_out, curr_activation, curr_opti)
    X_.append(i)
    Y_.append(accu)

embed_dim_li = [X_, Y_]

with open("embed_dim.json", "w+") as outfile:
    json.dump(embed_dim_li, outfile)

X_ = []
Y_ = []

max_features = 1000
embed_dim = 196
lstm_out = 128
curr_activation="softmax"
curr_opti = "adam"
for i in range(50, 500, 50):
    embed_dim = int(i)
    accu = gru(max_features, embed_dim, lstm_out, curr_activation, curr_opti)
    X_.append(i)
    Y_.append(accu)

embed_dim_li = [X_, Y_]

with open("embed_dim.json", "w+") as outfile:
    json.dump(embed_dim_li, outfile)

X_ = []
Y_ = []

max_features = 1000
embed_dim = 196
lstm_out = 128
curr_activation="softmax"
curr_opti = "adam"
for i in range(50, 500, 50):
    lstm_out = int(i)
    accu = gru(max_features, embed_dim, lstm_out, curr_activation, curr_opti)
    X_.append(i)
    Y_.append(accu)

lstm_out_li = [X_, Y_]

with open("gru_out.json", "w+") as outfile:
    json.dump(lstm_out_li, outfile)

X_ = []
Y_ = []

max_features = 1000
embed_dim = 196
lstm_out = 128
curr_activation="softmax"
curr_opti = "adam"
act = ["softmax", "sigmoid", "tanh", "relu"]

for i in range(4):
    curr_activation = act[int(i)]
    accu = gru(max_features, embed_dim, lstm_out, curr_activation, curr_opti)
    X_.append(i)
    Y_.append(accu)

act_li = [X_, Y_]

with open("act.json", "w+") as outfile:
    json.dump(act_li, outfile)

X_ = []
Y_ = []

max_features = 1000
embed_dim = 196
lstm_out = 128
curr_activation="softmax"
curr_opti = "adam"
opti = ["sgd", "nadam", "adam", "adamax", "adadelta", "rmsprop"]

for i in range(6):
    curr_opti = opti[int(i)]
    accu = gru(max_features, embed_dim, lstm_out, curr_activation, curr_opti)
    X_.append(i)
    Y_.append(accu)

opti_li = [X_, Y_]

with open("opti.json", "w+") as outfile:
    json.dump(opti_li, outfile)
