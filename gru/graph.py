import json 
import matplotlib.pyplot as plt
import numpy as np
import subprocess
import sys

s = sys.argv[1]

with open(s+".json", "r") as outfile:
    data = json.load(outfile)

X = data[0]

Y = data[1]
plt.xlabel(s)
plt.ylabel("accuracies")

plt.title("variation of accuracies by varying hyper-parameter " + s)

plt.plot(X, Y, color="blue")
plt.legend()
plt.savefig(s + ".png")
